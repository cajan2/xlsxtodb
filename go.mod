module gitee.com/cajan2/xlsxtodb

go 1.19

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.10.4
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
)
