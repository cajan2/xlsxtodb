package convert

import (
	"database/sql"
	"encoding/base64"
	"errors"
	"gitee.com/cajan2/xlsxtodb/pkg/set"
	"gitee.com/cajan2/xlsxtodb/pkg/utils"
	"strconv"
	"strings"
	"time"
)

func GetTableColumns(db *sql.DB, driverName, tableName string) (tableColumnMap map[int]string, err error) {
	sql := "SELECT * FROM " + utils.EscapeString(driverName, tableName) + " LIMIT 1;"
	dbRows, err := db.Query(sql)
	defer dbRows.Close()
	utils.Checkerr2(err, sql)
	//获取数据表字段名
	tableColumnMap = make(map[int]string, 0)
	if cols, err := dbRows.Columns(); err == nil {
		for index, col := range cols {
			if !utils.HasChineseChar(col) {
				tableColumnMap[index+1] = col
			}
		}
	}
	//tableColumnMap = onlyEnglishColumnMap
	utils.Checkerr2(err, sql)
	return
}

func GetUpdateSql(driverName, tableName string, fieldNames []string, values []string,
	needConflictOnFields string, updatedFieldSet *set.StringSet,
	distinctExcludedFieldSet *set.StringSet) (sql string, updateSetSql string, whereSql string) {
	sql = "INSERT INTO  " + utils.EscapeString(driverName, tableName)
	sql += " (\"" + strings.Join(fieldNames, "\",\"") + "\")"
	sql += " VALUES (" + strings.Join(values, ",") + ")"
	conflictFieldSet := set.NewFromSlice(strings.Split(needConflictOnFields, ","))
	updatedFieldSet = updatedFieldSet.Difference(conflictFieldSet).Difference(NeedNotUpdateFieldSet)
	distinctExcludedFieldSet = distinctExcludedFieldSet.Difference(conflictFieldSet)
	if conflictFieldSet.Len() > 0 && distinctExcludedFieldSet.Len() > 0 {
		for idx, fieldName := range updatedFieldSet.List() {
			if idx > 0 {
				updateSetSql += ","
			} else {
				updateSetSql += " "
			}
			updateSetSql += fieldName + "=excluded." + fieldName
		}
		for idx, fieldName := range distinctExcludedFieldSet.List() {
			//where	tbl.c3 is distinct from excluded.c3 or tbl.c4 is distinct from excluded.c4;
			if idx > 0 {
				whereSql += " or "
			} else {
				whereSql += " where "
			}
			whereSql += utils.EscapeString(driverName, tableName) + "." + fieldName +
				" is distinct from" + " excluded." + fieldName
		}
	}

	return sql, updateSetSql, whereSql
}

//解析Excel及数据库字段
func (c *Columns) ParseColumns() (err error) {
	c.useColumns = make(map[int][]string)
	hitTableColumnSet := set.New()
	allTableColmnSet := set.New()
	for key, valueSourceColumn := range c.sourceColumns {
		thiscolumn := valueSourceColumn.String()
		if thiscolumn == "" || utils.HasChineseChar(thiscolumn) {
			// 忽略中文字符的字段名
			continue
		}
		columnval := strings.Split(thiscolumn, "|")
		if columnval == nil || len(columnval) == 0 {
			continue
		}
		if columnval[0] == ":other" {
			c.useColumns[key] = columnval
		} else {
			for _, valueTableColumn := range c.tableColumnMap {
				allTableColmnSet.Add(valueTableColumn)
				if columnval[0] == valueTableColumn {
					c.useColumns[key] = columnval
					hitTableColumnSet.Add(valueTableColumn)
				}
			}
		}
	}

	if len(c.useColumns) < 1 {
		err = errors.New("数据表与xlsx表格不对应")
		return
	}
	missTableColumnSet := allTableColmnSet.Difference(hitTableColumnSet)
	generateFieldSet := missTableColumnSet.Intersect(NeedGenerateFieldSet)
	keyNext := len(c.sourceColumns) + 1
	for _, v := range generateFieldSet.List() {
		c.useColumns[keyNext] = []string{v, "generate"}
		keyNext += 1
	}
	return
}

//解析内容
func ParseValue(val string) (result string, needUpdate bool) {
	vals := strings.Split(val, "|")
	needUpdate = false
	switch vals[0] {
	case ":null":
		result = ""
	case ":time":
		result = strconv.Itoa(int(time.Now().Unix()))
	case ":datetime":
		result = time.Now().Format("2006-01-02 15:04:05")
	case ":random":
		result = strings.Replace(Substr(base64.StdEncoding.EncodeToString(utils.Krand(32,
			utils.KC_RAND_KIND_ALL)), 0, 32), "+/", "_-", -1)
	default:
		result = strings.TrimSpace(val)
		needUpdate = true
	}
	if len(vals) > 1 && strings.Contains(val, "update") {
		needUpdate = true
	}
	return
}

//按长度截取字符串
func Substr(str string, start int, length int) string {
	rs := []rune(str)
	rl := len(rs)
	end := 0

	if start < 0 {
		start = rl - 1 + start
	}
	end = start + length

	if start > end {
		start, end = end, start
	}

	if start < 0 {
		start = 0
	}
	if start > rl {
		start = rl
	}
	if end < 0 {
		end = 0
	}
	if end > rl {
		end = rl
	}

	return string(rs[start:end])
}
